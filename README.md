Create a REST API with CRUD features using any programming languages you prefer and any web framework.

Required features: list items, add, remove and delete item

•	create service and repository layer using in any sql/nosql storage with your own data model.
•	create API functional test to ensure the feature is working.
•	develop the task with the mindset that it must be ready for production.
•	a great plus if the app is deployed to hosting (heroku/aws/azure/digital ocean).
•	please push the source code into your github repository and we will review the codes.

Assessment points:
•	REST API routing design → 5 points
Application modelling and abstraction skill → 5 points
•	Quality assessment with functional API test (optional) → 3 points
•	Deployment to cloud infrastructure (optional) → 3 points

Case study:
1.	Product and category management. a product can belong to a category and the category can have hierarchical structure.
2.	Passing criteria: 7 points can be spread around all components, mandatory components minimum score 5.
3.	Please push your work to your Github repository
