<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});


$router->group(['namespace' => 'Category'], function () use ($router) {
	$router->get('/categories', 'CategoryController@index'); //list categories
	$router->get('/categories/{id}', 'CategoryController@show'); //show category by id (category ID)
	$router->post('/categories', 'CategoryController@store'); //add new category
	$router->post('/categories/{id}', 'CategoryController@createchild'); //add new category child from parent id as id (category ID)
	$router->put('/categories/{id}', 'CategoryController@update'); //update category
	$router->delete('/categories/{id}', 'CategoryController@destroy'); //delete category
});

$router->group(['namespace' => 'Product'], function () use ($router) {
	$router->get('/products', 'ProductController@index'); //list products
	$router->get('/products/{id}', 'ProductController@show'); //list product by id (product ID)
	$router->post('/products', 'ProductController@store'); //add new product
	$router->put('/products/{id}', 'ProductController@update'); //update product
	$router->delete('/products/{id}', 'ProductController@destroy'); //delete product
});
