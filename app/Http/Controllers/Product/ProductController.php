<?php
namespace App\Http\Controllers\Product;
use App\Http\Controllers\Controller;
use App\Product;
use App\Category;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
		
    }
	
	public function index(){
		$products = Product::leftJoin('categories','products.category_id', '=', 'categories.id')
		->select('products.*','categories.category_name')
		->get();
		if ($products->count() > 0) {
			$response=[
        		'error'=>false,
        		'message'=> 'All Products',
        		'data'=> $products,
    		];
        } else {
				$response=[
        		'error'=>false,
        		'message'=> 'Products Empty',
        		'data'=> NULL,
    		];
        }
	return response()->json($response);
	}
	
	public function show($id){
		$product = Product::leftJoin('categories','products.category_id', '=', 'categories.id')
		->select('products.*','categories.category_name')
		->where('products.id',$id)
		->first();
		if (!empty($product)) {
			$response=[
        		'error'=>false,
        		'message'=> 'Detail Product ID : '.$id.'',
        		'data'=> $product,
    		];
        } else {
				$response=[
        		'error'=>false,
        		'message'=> 'Product ID : '.$id.' Not Found',
        		'data'=> NULL,
    		];
        }
	return response()->json($response);
	}
	
	public function store(Request $request){
		$this->validate($request, [
			'category_id' => 'required',
			'name' => 'required',
			'price' => 'required',
		]);
		$data = new Product();
		$data->category_id = $request->input('category_id');
		$data->name = $request->input('name');
		$data->price = $request->input('price');
		$data->save();
		$response=[
        		'error'=>false,
        		'message'=> 'Create New Product',
				'data'=> $data
    		];
	return response()->json($response);
	}
	
	public function update(Request $request, $id){
		$this->validate($request, [
			'category_id' => 'required',
			'name' => 'required',
			'price' => 'required',
		]);
		$data = Product::where('id',$id)->first();
		$data->category_id = $request->input('category_id');
		$data->name = $request->input('name');
		$data->price = $request->input('price');
		$data->save();
		$response=[
        		'error'=>false,
        		'message'=> 'Update Product ID : '.$id.'',
				'data'=> $data
    		];
	return response()->json($response);
	}

	public function destroy($id){
		$data = Product::where('id',$id)->first();
		if (!empty($data)) {
			$data->delete();
			$response=[
        		'error'=>false,
        		'message'=> 'Delete Product ID : '.$id.'',
        		'data'=> $data,
    		];
        } else {
				$response=[
        		'error'=>false,
        		'message'=> 'Product ID : '.$id.' Not Found'
    		];
        }
	return response()->json($response);
	}
}
