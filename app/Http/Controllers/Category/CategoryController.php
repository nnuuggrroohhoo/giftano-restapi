<?php
namespace App\Http\Controllers\Category;
use App\Http\Controllers\Controller;
use App\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }
	
	public function index(){
		$category = Category::all();
		foreach ($category as $key => $val) {
			$source[$val->id]=  array(
				'id' => $val->id,
				'category_name' => $val->category_name,
				'parent_id' => $val->parent
			);
		}
		
		if ($category->count() > 0) {
			$response=[
        		'error'=>false,
        		'message'=> 'All Category',
        		'data'=> $this->makeparentchild($source),
    		];
        } else {
				$response=[
        		'error'=>false,
        		'message'=> 'Category Empty',
        		'data'=> NULL,
    		];
        }
		
	return response()->json($response);
	}
	
	function makeparentchild($source) {
		$result = array();
			foreach($source as &$v){
				if(is_null($v['parent_id'])){
					$result[] = &$v;
				}else{
					$parent_id = $v['parent_id'];
					if(isset($source[$parent_id])) {
						if(!isset($source[$parent_id]['children'])){
							$source[$parent_id]['children'] = array();
						}
						$source[$parent_id]['children'][] = &$v;
					}
				}
			}
	return $result;
	}
			
	public function show($id){
		$category = Category::where('id',$id)->first();
		if (!empty($category)) {
			$response=[
        		'error'=>false,
        		'message'=> 'Detail Category ID : '.$id.'',
        		'data'=> $category
    		];
        } else {
			$response=[
        		'error'=>false,
        		'message'=> 'Category ID : '.$id.' Not Found',
        		'data'=> NULL
    		];
        }
	return response()->json($response);
	}
	
	public function store(Request $request){
		$this->validate($request, [
			'category_name' => 'required'
		]);
		$data = new Category();
		$data->category_name = $request->input('category_name');
		$data->parent = $request->input('parent') ? $request->input('parent') : null;
		$data->save();
		$response=[
        		'error'=>false,
        		'message'=> 'Create New Category',
				'data'=> $data
    		];
	return response()->json($response);
	}
	
	public function createchild(Request $request, $id){
		$this->validate($request, [
			'category_name' => 'required'
		 ]);
		$data = Category::where('id',$id)->first();
		$data = new Category();
		$data->parent = $id;
		$data->category_name = $request->input('category_name');
		$data->save();
		$response=[
        		'error'=>false,
        		'message'=> 'Create New Category Child (Category ID : '.$id.')',
				'data'=> $data
    		];
	return response()->json($response);
	}
	
	public function update(Request $request, $id){
		$this->validate($request, [
			'category_name' => 'required',
		 ]);
		$data = Category::where('id',$id)->first();
		$data->category_name = $request->input('category_name');
		$data->parent = $request->input('parent') ? $request->input('parent') : null;
		$data->save();
		$response=[
        		'error'=>false,
        		'message'=> 'Update Category ID : '.$id.'',
				'data'=> $data
    		];
	return response()->json($response);
	}

	public function destroy($id){
		$data = Category::where('id',$id)->first();
		if (!empty($data)) {
			$data->delete();
			$response=[
        		'error'=>false,
        		'message'=> 'Delete Category ID : '.$id.'',
        		'data'=> $data,
    		];
        } else {
				$response=[
        		'error'=>false,
        		'message'=> 'Category ID : '.$id.' Not Found'
    		];
        }
	return response()->json($response);
	}
}
